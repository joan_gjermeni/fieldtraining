<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

$mod_strings = Array(
	'FieldTraining'          => 'Field Training',
	'Name'                   => 'Name',
	'SINGLE_FieldTraining'   => 'Field Training',
	'FieldTraining ID'       => 'Field Training ID',
	'Session'                => 'Session',
	'Field Training No'      => 'Field Training No',
	'Number of Participants' => 'Number of Participants',
	'Duration'               => 'Duration',
	'Start Date'             => 'Start Date',
	'End Date'               => 'End Date',
	'Skills'                 => 'Skills',
	'Employees'              => 'Employees',
	'Location'               => 'Location',
	'Scope'                  => 'Scope',

	'LBL_FIELD_TRAINING_INFORMATION' => 'Field Training Information',
	'LBL_PERSONALIZED_INFORMATION'   => 'Personalized Information',
	'LBL_DESCRIPTION_INFORMATION'    => 'Description',

);

?>
